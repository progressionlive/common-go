package token

import (
	"fmt"
	"gopkg.in/square/go-jose.v2"
	"gopkg.in/square/go-jose.v2/json"
	"gopkg.in/square/go-jose.v2/jwt"
	"net/http"
	"sync"
)

func Parse(token string, dest ...interface{}) error {

	parsed, err := jwt.ParseSigned(token)
	if err != nil {
		return err
	}

	err = parsed.UnsafeClaimsWithoutVerification(dest...)
	if err != nil {
		return err
	}

	return nil
}

func ParseSigned(tokenString, jwksURL string, dest ...interface{}) error {

	parsed, err := jwt.ParseSigned(tokenString)
	if err != nil {
		return err
	}

	jwk, err := findKey(parsed.Headers[0].KeyID, jwksURL)
	if err != nil {
		return err
	}

	err = parsed.Claims(jwk.Key, dest...)
	if err != nil {
		return err
	}

	return nil
}

var cache = make(map[string]*jose.JSONWebKey)
var cacheMutex sync.RWMutex

func findKey(jwkID, jwksURL string) (*jose.JSONWebKey, error) {

	k := jwksURL + "/" + jwkID

	cacheMutex.RLock()
	jwk, keyFound := cache[k]
	cacheMutex.RUnlock()
	if keyFound {
		return jwk, nil
	}

	cacheMutex.Lock()
	defer cacheMutex.Unlock()

	jwk, keyFound = cache[k]
	if keyFound {
		return jwk, nil
	}

	res, err := http.Get(jwksURL)
	if err != nil {
		return nil, err
	}
	//noinspection GoUnhandledErrorResult
	defer res.Body.Close()

	jwks := jose.JSONWebKeySet{}
	err = json.NewDecoder(res.Body).Decode(&jwks)
	if err != nil {
		return nil, err
	}

	for i, _ := range jwks.Keys {
		if jwks.Keys[i].KeyID == jwkID {
			jwk = &jwks.Keys[i]
			break
		}
	}

	if jwk == nil {
		return nil, fmt.Errorf("key %q not found", jwkID)
	}

	cache[k] = jwk

	return jwk, nil
}
