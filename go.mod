module bitbucket.org/progressionlive/common-go

go 1.13

require (
	github.com/golang/protobuf v1.4.3 // indirect
	github.com/gorilla/sessions v1.2.1
	golang.org/x/crypto v0.0.0-20201208171446-5f87f3452ae9 // indirect
	golang.org/x/net v0.0.0-20201209123823-ac852fbbde11 // indirect
	golang.org/x/oauth2 v0.0.0-20201208152858-08078c50e5b5
	google.golang.org/appengine v1.6.7 // indirect
	gopkg.in/square/go-jose.v2 v2.5.1
)
