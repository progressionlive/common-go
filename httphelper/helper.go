package httphelper

import (
	"encoding/json"
	"net/http"
	"strings"
)

func Error(w http.ResponseWriter, r *http.Request, code int, err error) {
	w.WriteHeader(code)
	accept := r.Header.Get("Accept")
	if strings.HasPrefix(accept, "application/json") {
		//goland:noinspection GoUnhandledErrorResult
		json.NewEncoder(w).Encode(map[string]interface{}{
			"error": err.Error(),
		})
	} else {
		//goland:noinspection GoUnhandledErrorResult
		w.Write([]byte(err.Error()))
	}
}

func Redirect(w http.ResponseWriter, r *http.Request, code int, location string) {
	w.Header().Add("Location", location)
	w.WriteHeader(code)
}
