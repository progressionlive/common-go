package oauth

import (
	"bitbucket.org/progressionlive/common-go/httphelper"
	"context"
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/sessions"
	"golang.org/x/oauth2"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type WebClientConfig struct {
	ApiUrl                url.URL
	ApiToken              string
	ApiTokenExpiry        time.Time
	OAuthConfig           *oauth2.Config
	SessionCookieName     string
	SessionCookieCodecKey []byte
}

type WebClient struct {
	Config      *WebClientConfig
	CookieStore *sessions.CookieStore
}

func NewClient(config *WebClientConfig) *WebClient {

	client := WebClient{}

	client.Config = config

	client.CookieStore = sessions.NewCookieStore(config.SessionCookieCodecKey)
	client.CookieStore.Options.Path = "/"
	client.CookieStore.Options.MaxAge = 0
	client.CookieStore.Options.HttpOnly = true
	if strings.Contains(config.OAuthConfig.RedirectURL, "localhost") {
		client.CookieStore.Options.Secure = false
		client.CookieStore.Options.SameSite = http.SameSiteDefaultMode
	} else {
		client.CookieStore.Options.Secure = true
		client.CookieStore.Options.SameSite = http.SameSiteNoneMode
	}

	return &client
}

func (c *WebClient) AuthCodeFlowStartMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		var err error

		state := r.URL.Query().Get("state")
		realm := r.URL.Query().Get("realm")
		persona := r.URL.Query().Get("persona")

		if state == "" {
			state, err = nonce(16)
			if err != nil {
				httphelper.Error(w, r, http.StatusInternalServerError, err)
				return
			}
		}

		// ignore error, we want a new session if old one is corrupt
		session, _ := c.CookieStore.Get(r, c.Config.SessionCookieName)

		session.Values["state"] = state

		err = session.Save(r, w)
		if err != nil {
			httphelper.Error(w, r, http.StatusInternalServerError, err)
			return
		}

		authCodeURL := c.Config.OAuthConfig.AuthCodeURL(state, oauth2.SetAuthURLParam("realm", realm), oauth2.SetAuthURLParam("persona", persona))

		next.ServeHTTP(w, r)

		httphelper.Redirect(w, r, http.StatusTemporaryRedirect, authCodeURL)
		return
	})
}

func (c *WebClient) AuthCodeFlowCallbackMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		code := r.URL.Query().Get("code")
		actualState := r.URL.Query().Get("state")

		// ignore error, we want a new session if old one is corrupt
		session, _ := c.CookieStore.Get(r, c.Config.SessionCookieName)

		expectedState, expectedStateOk := session.Values["state"].(string)
		if !expectedStateOk || expectedState != actualState {
			httphelper.Error(w, r, http.StatusBadRequest, fmt.Errorf("unexpected state"))
			return
		}

		token, err := c.Config.OAuthConfig.Exchange(r.Context(), code)
		if err != nil {
			httphelper.Error(w, r, http.StatusInternalServerError, err)
			return
		}

		delete(session.Values, "state")
		writeTokenToSession(session, token)

		err = session.Save(r, w)
		if err != nil {
			httphelper.Error(w, r, http.StatusInternalServerError, err)
			return
		}

		r = r.WithContext(context.WithValue(r.Context(), "token", token))

		next.ServeHTTP(w, r)
	})
}

func (c *WebClient) AuthCookieTokenMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// ignore error, we want a new session if old one is corrupt
		session, _ := c.CookieStore.Get(r, c.Config.SessionCookieName)

		token := readTokenFromSession(session)
		if token != nil {
			httphelper.Error(w, r, http.StatusUnauthorized, fmt.Errorf("authorization required"))
		}

		tokenSource := c.Config.OAuthConfig.TokenSource(r.Context(), token)

		tokenAfterRefresh, err := tokenSource.Token()
		if err != nil {
			httphelper.Error(w, r, http.StatusUnauthorized, err)
			return
		}

		if token.AccessToken != tokenAfterRefresh.AccessToken {

			writeTokenToSession(session, tokenAfterRefresh)

			err = session.Save(r, w)
			if err != nil {
				httphelper.Error(w, r, http.StatusInternalServerError, err)
				return
			}

			token = tokenAfterRefresh
		}

		r = r.WithContext(context.WithValue(r.Context(), "token", token))

		next.ServeHTTP(w, r)
	})
}

func (c *WebClient) AuthBearerTokenMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		authorization := r.Header.Get("Authorization")
		if authorization == "" {
			httphelper.Error(w, r, http.StatusUnauthorized, fmt.Errorf("authorization required"))
			return
		}

		authorizationParts := strings.SplitN(authorization, " ", 2)
		if strings.ToLower(authorizationParts[0]) != "bearer" {
			httphelper.Error(w, r, http.StatusUnauthorized, fmt.Errorf("authorization required"))
			return
		}
		accessToken := authorizationParts[1]

		token := &oauth2.Token{AccessToken: accessToken}

		r = r.WithContext(context.WithValue(r.Context(), "token", token))

		next.ServeHTTP(w, r)
	})
}

func (c *WebClient) AuthTokenMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		authorization := r.Header.Get("Authorization")
		if authorization != "" {
			authorizationParts := strings.SplitN(authorization, " ", 2)
			if strings.ToLower(authorizationParts[0]) == "bearer" {
				accessToken := authorizationParts[1]
				token := &oauth2.Token{AccessToken: accessToken}
				r = r.WithContext(context.WithValue(r.Context(), "token", token))
				next.ServeHTTP(w, r)
				return
			}
		}

		// ignore error, we want a new session if old one is corrupt
		session, _ := c.CookieStore.Get(r, c.Config.SessionCookieName)
		token := readTokenFromSession(session)
		if token != nil {
			tokenSource := c.Config.OAuthConfig.TokenSource(r.Context(), token)
			tokenAfterRefresh, err := tokenSource.Token()
			if err != nil {
				httphelper.Error(w, r, http.StatusUnauthorized, err)
				return
			}
			if token.AccessToken != tokenAfterRefresh.AccessToken {
				writeTokenToSession(session, tokenAfterRefresh)
				err = session.Save(r, w)
				if err != nil {
					httphelper.Error(w, r, http.StatusInternalServerError, err)
					return
				}
				token = tokenAfterRefresh
			}
			r = r.WithContext(context.WithValue(r.Context(), "token", token))
			next.ServeHTTP(w, r)
			return
		}

		httphelper.Error(w, r, http.StatusUnauthorized, fmt.Errorf("authorization required"))
		return

	})
}

func readTokenFromSession(session *sessions.Session) *oauth2.Token {
	accessToken, accessTokenOk := session.Values["access_token"].(string)
	if !accessTokenOk {
		return nil
	}
	token := &oauth2.Token{AccessToken: accessToken}
	tokenType, tokenTypeOk := session.Values["token_type"].(string)
	if tokenTypeOk {
		token.TokenType = tokenType
	}
	refreshToken, refreshTokenOk := session.Values["refresh_token"].(string)
	if refreshTokenOk {
		token.RefreshToken = refreshToken
	}
	expiry, expiryOk := session.Values["expiry"].(time.Time)
	if expiryOk {
		token.Expiry = expiry
	}
	idToken, idTokenOk := session.Values["id_token"].(string)
	if idTokenOk {
		token = token.WithExtra(map[string]interface{}{"id_token": idToken})
	}
	return token
}

func writeTokenToSession(session *sessions.Session, token *oauth2.Token) {
	session.Values["access_token"] = token.AccessToken
	session.Values["token_type"] = token.TokenType
	session.Values["refresh_token"] = token.RefreshToken
	session.Values["expiry"] = token.Expiry
	session.Values["id_token"] = token.Extra("id_token")
}

func GetContextToken(r *http.Request) (*oauth2.Token, error) {
	tokens, tokensOk := r.Context().Value("token").(*oauth2.Token)
	if !tokensOk {
		return nil, fmt.Errorf("no context token found")
	}
	return tokens, nil
}

func nonce(size int) (string, error) {
	buf := make([]byte, size)
	_, err := rand.Read(buf)
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(buf), nil
}

func (c *WebClient) CallApi(path string, values map[string]string) (decodedResponse interface{}, err error) {

	if c.Config.ApiToken == "" || time.Now().After(c.Config.ApiTokenExpiry) {
		err = c.fecthAccessToken()
		if err != nil {
			return
		}
	}

	apiUrl, err := url.Parse(c.Config.ApiUrl.String() + path)
	if err != nil {
		return
	}

	if values != nil {
		query := apiUrl.Query()
		for key, element := range values {
			query.Set(key, element)
		}
		apiUrl.RawQuery = query.Encode()
	}

	req, err := http.NewRequest("GET", apiUrl.String(), nil)
	if err != nil {
		return
	}
	req.Header.Add("authorization", "Bearer "+c.Config.ApiToken)
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return
	}

	defer res.Body.Close()
	err = json.NewDecoder(res.Body).Decode(&decodedResponse)
	return
}

func (c *WebClient) fecthAccessToken() error {

	payload := map[string]interface{}{
		"client_id":     c.Config.OAuthConfig.ClientID,
		"client_secret": c.Config.OAuthConfig.ClientSecret,
		"audience":      c.Config.ApiUrl.String(),
		"grant_type":    "client_credentials",
	}

	jsonPayload, err := json.Marshal(payload)
	if err != nil {
		return err
	}
	reader := strings.NewReader(string(jsonPayload))
	req, err := http.NewRequest("POST", c.Config.OAuthConfig.Endpoint.TokenURL, reader)
	if err != nil {
		return err
	}
	req.Header.Add("content-type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	defer res.Body.Close()
	var response map[string]interface{}
	err = json.NewDecoder(res.Body).Decode(&response)
	if err != nil {
		return err
	}

	if response["access_token"] == nil {
		return errors.New("no access token found")
	}
	c.Config.ApiToken = response["access_token"].(string)
	expiresIn := time.Duration(response["expires_in"].(float64))
	c.Config.ApiTokenExpiry = time.Now().Add(expiresIn * time.Second)
	return nil
}
