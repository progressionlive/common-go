package proxy

import (
	"bitbucket.org/progressionlive/common-go/httphelper"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
)

type Director func(*http.Request)

type TargetFinder func(http.ResponseWriter, *http.Request) (*url.URL, error)

type TransportFinder func(http.ResponseWriter, *http.Request) (http.RoundTripper, error)

func PathReplacer(local, remote string) Director {
	return func(r *http.Request) {
		r.URL.Path = strings.Replace(r.URL.Path, local, remote, 1)
	}
}

func ForwardHandler(director Director, targetFinder TargetFinder, transportFinder TransportFinder) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		target, err := targetFinder(w, r)
		if err != nil {
			httphelper.Error(w, r, http.StatusInternalServerError, err)
			return
		}

		transport, err := transportFinder(w, r)
		if err != nil {
			httphelper.Error(w, r, http.StatusInternalServerError, err)
			return
		}

		proxy := httputil.NewSingleHostReverseProxy(target)

		proxy.Director = func(req *http.Request) {
			req.URL.Scheme = target.Scheme
			req.URL.Host = target.Host
			req.Header.Set("X-Forwarded-Host", req.Host)
			req.Host = target.Host
			if target.RawQuery == "" || req.URL.RawQuery == "" {
				req.URL.RawQuery = target.RawQuery + req.URL.RawQuery
			} else {
				req.URL.RawQuery = target.RawQuery + "&" + req.URL.RawQuery
			}
			if _, ok := req.Header["User-Agent"]; !ok {
				req.Header.Set("User-Agent", "")
			}
			director(req)
		}

		proxy.Transport = transport

		proxy.ServeHTTP(w, r)
	}
}
