package plive

import (
	"bitbucket.org/progressionlive/common-go/oauth"
	"bitbucket.org/progressionlive/common-go/token"
	"golang.org/x/oauth2"
	"gopkg.in/square/go-jose.v2/jwt"
	"net/http"
	"net/url"
	"strings"
)

type ProgressionClient struct {
	HttpClient *http.Client
	BaseURL    string
}

func ParseToken(tokenString string) (subject, audience string, err error) {

	claims := jwt.Claims{}
	err = token.Parse(tokenString, &claims)
	if err != nil {
		return
	}

	subject = claims.Subject
	audience = claims.Audience[0]

	return
}

func ParseSignedToken(tokenString string, jwksURL string) (subject, audience string, err error) {

	claims := jwt.Claims{}
	err = token.ParseSigned(tokenString, jwksURL, &claims)
	if err != nil {
		return
	}

	subject = claims.Subject
	audience = claims.Audience[0]

	return
}

func ParseSubject(subject string) (server, cieUid, userId string, err error) {

	parts := strings.SplitN(subject, "/", 3)

	server = parts[0]
	cieUid = parts[1]
	userId = parts[2]

	return
}

func ContextTokenAudienceTargetFinder(w http.ResponseWriter, r *http.Request) (*url.URL, error) {

	tokens, err := oauth.GetContextToken(r)
	if err != nil {
		return nil, err
	}

	_, audience, err := ParseToken(tokens.AccessToken)
	if err != nil {
		return nil, err
	}

	audienceURL, err := url.Parse(audience)
	if err != nil {
		return nil, err
	}

	return audienceURL, nil
}

func ContextTokenOAuthTransportFinder(w http.ResponseWriter, r *http.Request) (http.RoundTripper, error) {

	tokens, err := oauth.GetContextToken(r)
	if err != nil {
		return nil, err
	}

	httpClient := oauth2.NewClient(r.Context(), oauth2.StaticTokenSource(tokens))

	return httpClient.Transport, nil
}

func ContextTokenProgressionClient(w http.ResponseWriter, r *http.Request) (*ProgressionClient, error) {

	tokens, err := oauth.GetContextToken(r)
	if err != nil {
		return nil, err
	}

	httpClient := oauth2.NewClient(r.Context(), oauth2.StaticTokenSource(tokens))

	_, progressionURL, err := ParseToken(tokens.AccessToken)
	if err != nil {
		return nil, err
	}

	pliveClient := ProgressionClient{
		HttpClient: httpClient,
		BaseURL:    progressionURL,
	}

	return &pliveClient, nil
}
